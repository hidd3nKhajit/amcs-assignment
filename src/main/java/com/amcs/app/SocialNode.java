package com.amcs.app;

import java.util.ArrayList;
import java.util.Optional;

public class SocialNode {
  private String name = "nil";
  private ArrayList<SocialNode> friends = new ArrayList<>();

  SocialNode() {}

  SocialNode(String name) {
    this.name = name;
  }

  SocialNode(String name, ArrayList<SocialNode> friends) {
    this.name = name;
    this.friends = friends;
  }

  public void addFriend(SocialNode friend) {
    this.friends.add(friend);
  }

  public void removeFriend(String deleteName) {
    for(int i = 0; i < this.friends.size(); i++) {
      if(this.friends.get(i).getName().equals(deleteName)) {
        this.friends.remove(i);
      }
    }
  }

  // getter setters
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<SocialNode> getFriends() {
    return friends;
  }

  public void setFriends(ArrayList<SocialNode> friends) {
    this.friends = friends;
  }

  public String toString() {
    String str = "Node Name: " + this.getName() + "\nFriend List:\n";
    for(int i = 0; i < this.friends.size(); i++) {
        str += (i+1) + ". " + this.friends.get(i).getName() + "\n";
    }
    str += "\n\n\n\n\n";
    return str;
  }
}
