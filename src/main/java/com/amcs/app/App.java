package com.amcs.app;

import org.graphstream.graph.Graph;
import org.graphstream.graph.Node;
import org.graphstream.graph.implementations.*;
import org.graphstream.ui.view.Viewer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Optional;
import java.util.Scanner;

import com.amcs.app.SocialNode;

public class App 
{
    public static void main( String[] args )
    {
        // sample printing and getting user input
        // Scanner scanner = new Scanner(System.in);
        // System.out.println("enter something");
        // String input = scanner.nextLine();
        // System.out.println(input);
        // demoTraverse();
        // demoGraph();
        // demoVertex();
        // demoAddEdge();
        demoRemoveEdge();
        // demoGraphFromNetwork();
    }

    static void demoRemoveEdge() {
        Scanner scanner = new Scanner(System.in);
        ArrayList<SocialNode> network = new ArrayList<>();

        // init network
        network.add(new SocialNode("ali"));
        network.add(new SocialNode("abu"));

        // make friends
        SocialNode aliNode = findNodeByName("ali", network).get();
        SocialNode abuNode = findNodeByName("abu", network).get();
        
        aliNode.addFriend(abuNode);
        abuNode.addFriend(aliNode);
        
        // printout
        System.out.println("Printing out network people and friend list... \n\n");
        for(SocialNode person : network) {
            System.out.println(person.toString());
        }

        // pause
        System.out.println("PAUSING");
        scanner.nextLine();

        // breakups
        System.out.println("Breaking up friendships...");

        HashMap<String, String> searched = new HashMap<>();
        Optional<SocialNode> result = traverser(aliNode, "abu", aliNode.getName(), searched);

        if(result.isPresent()) {
            System.out.println("FOUND the frienship");
            System.out.println("Commence destruction with unicorn magix...");
            aliNode.removeFriend("abu");
            result.get().removeFriend("ali");

            System.out.println("Destruction Complete!\n\n");
        }

        System.out.println("Printing out network people and friend list... \n\n");
        for(SocialNode person : network) {
            System.out.println(person.toString());
        }

        scanner.close();
    }

    static void demoAddEdge() {
        Scanner scanner = new Scanner(System.in);
        ArrayList<SocialNode> network = new ArrayList<>();
        Boolean exit = true;

        network.add(new SocialNode("ali"));
        network.add(new SocialNode("abu"));

        do {
            System.out.print("From?: ");
            String from = scanner.nextLine();

            if(from.equals("exit")) {
                System.out.println("Exiting Program... ");
                exit = false;
                break;
            }

            System.out.print("\n\nTo?: ");
            String to = scanner.nextLine();

            SocialNode fromNode = findNodeByName(to, network).get();
            SocialNode toNode = findNodeByName(from, network).get();

            toNode.addFriend(fromNode);

        } while (exit);
        
        System.out.println("Printing out network people and friend list... \n\n");
        for(SocialNode person : network) {
            System.out.println(person.toString());
        }
    }

    static Optional<SocialNode> findNodeByName(String name, ArrayList<SocialNode> network) {
        for(SocialNode person : network) {
            if(person.getName().equals(name)) {
                return Optional.of(person);
            }
        }
        return Optional.empty();
    }

    static void demoVertex() {
        Scanner scanner = new Scanner(System.in);
        ArrayList<SocialNode> network = new ArrayList<>();
        Boolean exit = true;
        do {
            
            System.out.print("Enter new Vertex Name: ");
            String input = scanner.nextLine();

            if(input.equals("exit")) {
                System.out.print("Exiting Program... ");
                exit = false;
                break;
            }

            network.add(new SocialNode(input));

            System.out.print("Done Add, " + input + "\n\n\n");
            
        } while (exit);
        scanner.close();
        System.out.println("Print out final network list of people...");
        for(SocialNode people : network) {
            System.out.println(people.getName());
        }
    }

    static void demoTraverse() {
        // init people on the network
        SocialNode bob = new SocialNode("bob");
        SocialNode mike = new SocialNode("mike");
        SocialNode emma = new SocialNode("emma");
        SocialNode jill = new SocialNode("jill");
        SocialNode john = new SocialNode("john");
        SocialNode shane = new SocialNode("shane");
        SocialNode leah = new SocialNode("leah");
        SocialNode liz = new SocialNode("liz");
        SocialNode allen = new SocialNode("allen");
        SocialNode lisa = new SocialNode("lisa");

        // connect them together
        // qwer.addFriend(asdf);
        // 

        mike.addFriend(bob);
        mike.addFriend(emma);
        mike.addFriend(jill);

        bob.addFriend(mike);
        bob.addFriend(emma);
        bob.addFriend(jill);
        bob.addFriend(john);

        leah.addFriend(john);
        leah.addFriend(jill);
        leah.addFriend(shane);

        shane.addFriend(leah);
        shane.addFriend(jill);
        shane.addFriend(emma);
        shane.addFriend(liz);
        shane.addFriend(john);

        liz.addFriend(emma);
        liz.addFriend(shane);
        liz.addFriend(allen);

        allen.addFriend(liz);
        allen.addFriend(lisa);

        lisa.addFriend(allen);

        emma.addFriend(mike);
        emma.addFriend(liz);
        emma.addFriend(shane);
        emma.addFriend(jill);
        emma.addFriend(bob);

        john.addFriend(bob);
        john.addFriend(jill);
        john.addFriend(shane);
        john.addFriend(leah);

        jill.addFriend(bob);
        jill.addFriend(mike);
        jill.addFriend(emma);
        jill.addFriend(john);
        jill.addFriend(leah);
        jill.addFriend(shane);
        
        // lets pretend entry point is "lisa"
        HashMap<String, String> searched = new HashMap<>();
        System.out.println("SEARCH STARTED ON: " + LocalDateTime.now().toString());
        Optional<SocialNode> result = traverser(john, "emma", lisa.getName(), searched);
        System.out.println("SEARCH ENDED ON: " + LocalDateTime.now().toString());
        System.out.println();

        if(result.isPresent()) {
            System.out.println("Found the dumbass");
            SocialNode dumbassNode = result.get();
            System.out.println("Printing SocialNode Name as proof: " + dumbassNode.getName());
        } else {
            System.out.println("dumbass is not Found...");
            System.exit(0);
        }  
        
    }

    // Breadth First Search (BFS))
    static Optional<SocialNode> traverser(SocialNode node, String dumbassName, String previousNode, HashMap<String, String> searched) {
        searched.put(node.getName(), node.getName());
        ArrayList<SocialNode> nodeFriends = node.getFriends();

        // check if this loser has any friends
        if(nodeFriends.size() > 0) {
            // check if any of the friends is the "dumbass" that we're looking for
            for(int i = 0; i < nodeFriends.size(); i++) {
                if(nodeFriends.get(i).getName().equals(dumbassName)) {
                    // if the friend is the dumbass, pack up and let's go
                    return Optional.of(nodeFriends.get(i));
                }
            }

            // ok so non of the friends is the dumb ass
            // check the friends' friends'
            for(int i = 0; i < nodeFriends.size(); i++) {
                if(searched.containsKey(nodeFriends.get(i).getName())) continue;

                // skip checking previous search to prevent infinite loop
                if(nodeFriends.get(i).getName().equals(previousNode)) {
                    continue;
                }

                // reuse traverser to find dumbass
                Optional<SocialNode> result = traverser(nodeFriends.get(i), dumbassName, nodeFriends.get(i).getName(), searched);
                // if dumbass is found, pack up and let's go
                if(result.isPresent()) {
                    return Optional.of(result.get());
                } else {
                    // else continue harras other friends for info
                    continue;
                }
            }

            // if dumbass is no where to found on this god damned planet
            // then he doesn't exist
            return Optional.empty();
        } else {
            // if loser has no friends, pack up and let's go
            return Optional.empty();
        }
    }

    static void demoGraphFromNetwork() {
        String NODE_STYLE = "size: 50px, 50px; fill-color: lightblue;";
        System.setProperty("org.graphstream.ui", "swing");
        Graph graph = new SingleGraph("I can see dead pixels");

        ArrayList<SocialNode> network = new ArrayList<>();
        SocialNode ali = new SocialNode("ali");
        SocialNode abu = new SocialNode("abu");
        ali.addFriend(abu);
        network.add(ali);
        network.add(abu);

        // add everyone on the network to graph first
        for (SocialNode person : network) {
            Node node = graph.addNode(person.getName());
            node.setAttribute("ui.style", NODE_STYLE);
            node.setAttribute("label", person.getName());
        }

        for (SocialNode person : network) {
            Node personGraphNode = graph.getNode(person.getName());
            for (SocialNode friend : person.getFriends()) {
                Node friendGraphNode = graph.getNode(friend.getName());
                String edgeId = person.getName() + friend.getName();
                graph.addEdge(edgeId, personGraphNode, friendGraphNode, false);
            }
        }

        graph.display(true);
    }

    static void demoGraph() {
        String NODE_STYLE = "size: 50px, 50px; fill-color: lightblue;";
        System.setProperty("org.graphstream.ui", "swing");

        Graph graph = new SingleGraph("I can see dead pixels");

        // template
        // Node shitNode = graph.addNode("asdf");
        // shitNode.setAttribute("ui.style", NODE_STYLE);
        // shitNode.setAttribute("label", "asdf");
        // 

        // Nodes
        Node bobNode = graph.addNode("Bob");
        bobNode.setAttribute("ui.style", NODE_STYLE);
        bobNode.setAttribute("label", "Bob");

        Node mikeNode = graph.addNode("Mike");
        mikeNode.setAttribute("ui.style", NODE_STYLE);
        mikeNode.setAttribute("label", "Mike");

        Node emmaNode = graph.addNode("Emma");
        emmaNode.setAttribute("ui.style", NODE_STYLE);
        emmaNode.setAttribute("label", "Emma");

        Node jillNode = graph.addNode("Jill");
        jillNode.setAttribute("ui.style", NODE_STYLE);
        jillNode.setAttribute("label", "Jill");
        
        Node johnNode = graph.addNode("John");
        johnNode.setAttribute("ui.style", NODE_STYLE);
        johnNode.setAttribute("label", "John");
        
        Node shaneNode = graph.addNode("Shane");
        shaneNode.setAttribute("ui.style", NODE_STYLE);
        shaneNode.setAttribute("label", "Shane");
        
        Node leahNode = graph.addNode("Leah");
        leahNode.setAttribute("ui.style", NODE_STYLE);
        leahNode.setAttribute("label", "Leah");

        // Edges
        graph.addEdge("0", mikeNode, bobNode, true);
        graph.addEdge("1", bobNode, johnNode, true);
        graph.addEdge("2", johnNode, leahNode, true);
        graph.addEdge("3", leahNode, shaneNode, true);
        graph.addEdge("4", shaneNode, emmaNode, true);
        graph.addEdge("5", emmaNode, mikeNode, true);
        graph.addEdge("6", mikeNode, jillNode, true);
        graph.addEdge("7", bobNode, emmaNode, true);
        graph.addEdge("8", johnNode, shaneNode, true);
        graph.addEdge("9", jillNode, leahNode, true);
        graph.addEdge("10", bobNode, jillNode, true);
        graph.addEdge("11", jillNode, emmaNode, true);
        graph.addEdge("12", jillNode, johnNode, true);
        graph.addEdge("13", jillNode, shaneNode, true);

        graph.display();
    }
}
